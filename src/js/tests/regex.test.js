const {describe} = require('mocha');
const reg = require('../regex');

describe('regex1', function () {
    it('Найти строки ahb, acb, aeb', function () {
        assert.deepEqual(reg.task1('ahb acb aeb aeeb adcb axeb'), ['ahb', 'acb', 'aeb']);
    });
});

describe('regex2', function () {
    it('Найти строки abba adca abea', function () {
        assert.deepEqual(reg.task2('aba aca aea abba adca abea'), ['abba', 'adca', 'abea']);
    });
});

describe('regex3', function () {
    it('Найти строки abba и abea', function () {
        assert.deepEqual(reg.task3('aba aca aea abba adca abea'), ['abba', 'abea']);
    });
});

describe('regex4', function () {
    it('Найти строки aba, abba, abbba', function () {
        assert.deepEqual(reg.task4('aa aba abba abbba abca abea'), ['aba', 'abba', 'abbba']);
    });
});

describe('regex5', function () {
    it('Найти строки aa, aba, abba, abbba', function () {
        assert.deepEqual(reg.task5('aa aba abba abbba abca abea'), ['aa', 'aba', 'abba', 'abbba']);
    });
});

describe('regex6', function () {
    it('Найти строки aa, aba', function () {
        assert.deepEqual(reg.task6('aa aba abba abbba abca abea'), ['aa', 'aba']);
    });
});

describe('regex7', function () {
    it('Найти строки aa, aba, abba, abbba', function () {
        assert.deepEqual(reg.task7('aa aba abba abbba abca abea'), ['aa', 'aba', 'abba', 'abbba']);
    });
});

describe('regex8', function () {
    it('Найти строки ab abab abab abababab abea', function () {
        assert.deepEqual(reg.task8('ab abab abab abababab abea'), ['ab', 'abab', 'abab', 'abababab', 'ab']);
    });
});

describe('regex9', function () {
    it('Найти строки a.a', function () {
        assert.deepEqual(reg.task9('a.a aba aea'), ['a.a']);
    });
});

describe('regex10', function () {
    it('Найти строки 2+3', function () {
        assert.deepEqual(reg.task10('2+3 223 2223'), ['2+3']);
    });
});

describe('regex11', function () {
    it('Найти строки 2+3, 2++3, 2+++3', function () {
        assert.deepEqual(reg.task11('23 2+3 2++3 2+++3 345 567'), ['2+3', '2++3', '2+++3']);
    });
});

describe('regex12', function () {
    it('Найти строки 23, 2+3, 2++3, 2+++3', function () {
        assert.deepEqual(reg.task12('23 2+3 2++3 2+++3 445 677'), ['23', '2+3', '2++3', '2+++3']);
    });
});

describe('regex13', function () {
    it('Найти строки *q+, *qq+, *qqq+', function () {
        assert.deepEqual(reg.task13('*+ *q+ *qq+ *qqq+ *qqq qqq+'), ['*q+', '*qq+', '*qqq+']);
    });
});

describe('regex14', function () {
    it('Найти строки, по краям которых стоят буквы "a", и заменит каждую из них на "!"', function () {
        assert.deepEqual(reg.task14('aba accca azzza wwwwa'), '!b!, !ccc!, !zzz!');
    });
});

describe('regex15', function () {
    it('Найти строки abba, abbba, abbbba и только их', function () {
        assert.deepEqual(reg.task15('aa aba abba abbba abbbba abbbbba'), ['abba', 'abbba', 'abbbba']);
    });
});

describe('regex16', function () {
    it('Найти строки вида aba, в которых \'b\' встречается менее 3-х раз (включительно).', function () {
        assert.deepEqual(reg.task16('aa aba abba abbba abbbba abbbbba'), ['aa', 'aba', 'abba', 'abbba']);
    });
});

describe('regex17', function () {
    it('Найти строки вида aba, в которых \'b\' встречается более 4-х раз (включительно)', function () {
        assert.deepEqual(reg.task17('aa aba abba abbba abbbba abbbbba'), ['abbbba', 'abbbbba']);
    });
});

describe('regex18', function () {
    it('Найти строки, в которых по краям стоят буквы \'a\', а между ними одна цифра', function () {
        assert.deepEqual(reg.task18('a1a a2a a3a a4a a5a aba aca'), ['a1a', 'a2a', 'a3a', 'a4a', 'a5a']);
    });
});

describe('regex19', function () {
    it('Найти строки, в которых по краям стоят буквы \'a\', а между ними любое количество цифр', function () {
        assert.deepEqual(reg.task19('a1a a22a a333a a4444a a55555a aba aca'), ['a1a', 'a22a', 'a333a', 'a4444a', 'a55555a']);
    });
});

describe('regex20', function () {
    it('Найти строки, в которых по краям стоят буквы \'a\', а между ними любое количество цифр (в том числе и ноль)', function () {
        assert.deepEqual(reg.task20('aa a1a a22a a333a a4444a a55555a aba aca'), ['aa', 'a1a', 'a22a', 'a333a', 'a4444a', 'a55555a']);
    });
});

describe('regex21', function () {
    it('Найти строки следующего вида: по краям стоят буквы \'a\' и \'b\', а между ними - не число', function () {
        assert.deepEqual(reg.task21('avb a1b a2b a3b a4b a5b abb acb'), ['avb', 'abb', 'acb']);
    });
});

describe('regex22', function () {
    it('Найти строки следующего вида: по краям стоят буквы \'a\' и \'b\', а между ними - не буква и не цифра', function () {
        assert.deepEqual(reg.task22('ave a#b a2b a$b a4b a5b a-b acb'), ['a#b', 'a$b', 'a-b']);
    });
});

describe('regex23', function () {
    it('Заменить все пробелы на \'!\'', function () {
        assert.deepEqual(reg.task23('ave a#a a2a a$a a4a a5a a-a aca'), 'ave!a#a!a2a!a$a!a4a!a5a!a-a!aca');
    });
});

describe('regex24', function () {
    it('Найти строки aba, aea, axa, не затронув остальных', function () {
        assert.deepEqual(reg.task24('aba aea aca aza axa'), ['aba', 'aea', 'axa']);
    });
});

describe('regex25', function () {
    it('Найти строки aba, a.a, a+a, a*a, не затронув остальных', function () {
        assert.deepEqual(reg.task25('aba aea aca aza axa a.a a+a a*a'), ['aba', 'a.a', 'a+a', 'a*a']);
    });
});

describe('regex26', function () {
    it('Найти строки следующего вида: по краям стоят буквы \'a\', а между ними - цифра от 3-х до 7-ми', function () {
        assert.deepEqual(reg.task26('a0a a2a a4a a6a a7a a.a a+a a*a'), ['a4a', 'a6a', 'a7a']);
    });
});

describe('regex27', function () {
    it('Найти строки следующего вида: по краям стоят буквы \'a\', а между ними - буква от a до g.', function () {
        assert.deepEqual(reg.task27('aba aea aca aza axa a.a a+a a*a'), ['aba', 'aea', 'aca']);
    });
});

describe('regex28', function () {
    it('Найти строки следующего вида: по краям стоят буквы \'a\', а между ними - буква от a до f и от j до z', function () {
        assert.deepEqual(reg.task28('aba aea aca aza axa a.a aha a*a'), ['aba', 'aea', 'aca', 'aza', 'axa']);
    });
});

describe('regex29', function () {
    it('Найти строки следующего вида: по краям стоят буквы \'a\', а между ними - буква от a до f и от A до Z', function () {
        assert.deepEqual(reg.task29('aba aea aCa aza aXa aOa aka ala'), ['aba', 'aea', 'aCa', 'aXa', 'aOa']);
    });
});

describe('regex30', function () {
    it('Найти строки следующего вида: по краям стоят буквы \'a\', а между ними - не \'e\' и не \'x\'', function () {
        assert.deepEqual(reg.task30('aba aea aca aza axa a-a a#a'), ['aba', 'aca', 'aza', 'a-a', 'a#a']);
    });
});

describe('regex31', function () {
    it('Найти строки следующего вида: по краям стоят буквы \'w\', а между ними - буква кириллицы', function () {
        assert.deepEqual(reg.task31('wйw wяw wёw wqw'), ['wйw', 'wяw', 'wёw']);
    });
});

describe('regex32', function () {
    it('Найти строки следующего вида: по краям стоят буквы \'a\', а между ними - маленькие латинские буквы, не затронув остальных', function () {
        assert.deepEqual(reg.task32('aAXa aeffa aGha aza ax23a a3sSa'), ['aeffa', 'aza']);
    });
});

describe('regex33', function () {
    it('Найти строки следующего вида: по краям стоят буквы \'a\', а между ними - маленькие и большие латинские буквы, не затронув остальных', function () {
        assert.deepEqual(reg.task33('aAXa aeffa aGha aza ax23a a3sSa'), ['aAXa', 'aeffa', 'aGha', 'aza']);
    });
});

describe('regex34', function () {
    it('Найти строки следующего вида: по краям стоят буквы \'a\', а между ними - маленькие латинские буквы и цифры, не затронув остальных', function () {
        assert.deepEqual(reg.task34('aAXa aeffa aGha aza ax23a a3sSa'), ['aeffa', 'aza', 'ax23a']);
    });
});

describe('regex35', function () {
    it('Найти все слова по шаблону: любая кириллическая буква любое количество раз', function () {
        assert.deepEqual(reg.task35('ааа ббб ёёё ззз ййй ААА БББ ЁЁЁ ЗЗЗ ЙЙЙ'), ['ааа', 'ббб', 'ёёё', 'ззз', 'ййй', 'ААА', 'БББ', 'ЁЁЁ', 'ЗЗЗ', 'ЙЙЙ']);
    });
});

describe('regex36', function () {
    it('Заменить первое \'aaa\' на \'!\'', function () {
        assert.deepEqual(reg.task36('aaa aaa aaa'), '! aaa aaa');
    });
});

describe('regex37', function () {
    it('Заменить первое \'aaa\' на \'!\'', function () {
        assert.deepEqual(reg.task37('aaa aaa aaa'), 'aaa aaa !');
    });
});

describe('regex38', function () {
    it('Найти строки следующего вида: по краям стоят буквы \'a\', а между ними - буква \'x\' или буква \'e\' любое количество раз', function () {
        assert.deepEqual(reg.task38('aeeea aeea aea axa axxa axxxa'), ['aeeea', 'aeea', 'aea', 'axa', 'axxa', 'axxxa']);
    });
});

describe('regex39', function () {
    it('Найти строки следующего вида: по краям стоят буквы \'a\', а между ними - или буква \'e\' два раза или буква \'x\' любое количество раз', function () {
        assert.deepEqual(reg.task39('aeeea aeea aea axa axxa axxxa'), ['aeea', 'axa', 'axxa', 'axxxa']);
    });
});

describe('regex40', function () {
    it('Заменить строку \'a\\a\' на \'!\'', function () {
        assert.deepEqual(reg.task40('a\\a abc'), '! abc');
    });
});

describe('regex41', function () {
    it('Заменить строку \'a\\\\\\a\' на \'!\'', function () {
        assert.deepEqual(reg.task41('a\\a a\\\a a\\\\a'), 'a\\a a\\\a !');
    });
});

describe('regex42', function () {
    it('Заменить содержимое всех конструкций /...\\ на \'!\'', function () {
        assert.deepEqual(reg.task42('bbb \/aaa\\ bbb \/ccc\\'), 'bbb ! bbb !');
    });
});

describe('regex43', function () {
    it('Поменять местами то, что стоит до @ на то, что стоит после нее', function () {
        assert.deepEqual(reg.task43('aaa@bbb eee7@kkk'), 'bbb@aaa kkk@eee7');
    });
});

describe('regex44', function () {
    it('Найти все цифры и удвоить их количество', function () {
        assert.deepEqual(reg.task44('a1b2c3'), 'a11b22c33');
    });
});

describe('regex45', function () {
    it('Определить, что переданная строка является емэйлом mymail@mail.ru', function () {
        assert.deepEqual(reg.task45('mymail@mail.ru'), true);
    });
    it('Определить, что переданная строка является емэйлом my.mail@mail.ru', function () {
        assert.deepEqual(reg.task45('my.mail@mail.ru'), true);
    });
    it('Определить, что переданная строка является емэйлом my-mail@mail.ru', function () {
        assert.deepEqual(reg.task45('my-mail@mail.ru'), true);
    });
    it('Определить, что переданная строка является емэйлом mail@mail.com', function () {
        assert.deepEqual(reg.task45('mail@mail.com'), true);
    });
    it('Определить, что переданная строка является емэйлом mail.com', function () {
        assert.deepEqual(reg.task45('mail.com'), false);
    });
});

describe('regex46', function () {
    it('Выделить из текста емейлы: "Примеры емэйлов для тестирования mymail@mail.ru, my.mail@mail.ru, my-mail@mail.ru, my_mail@mail.ru, mail@mail.com"',
        function () {
            assert.deepEqual(reg.task46('Примеры емэйлов для тестирования mymail@mail.ru, my.mail@mail.ru, my-mail@mail.ru, my_mail@mail.ru, mail@mail.com'),
                ['mymail@mail.ru', 'my.mail@mail.ru', 'my-mail@mail.ru', 'my_mail@mail.ru', 'mail@mail.com']);
        });
});

describe('regex47', function () {
    it('Определить, что переданная строка является доменом site.ru', function () {
        assert.deepEqual(reg.task47('site.ru'), true);
    });
    it('Определить, что переданная строка является доменом my-site.com', function () {
        assert.deepEqual(reg.task47('my-site.com'), true);
    });
    it('Определить, что переданная строка является доменом my-site', function () {
        assert.deepEqual(reg.task47('my-site'), false);
    });
});

describe('regex48', function () {
    it('Определить, что переданная строка является доменом http://site.ru', function () {
        assert.deepEqual(reg.task48('http://site.ru'), true);
    });
    it('Определить, что переданная строка является доменом my-site.com', function () {
        assert.deepEqual(reg.task48('my-site.com'), false);
    });
    it('Определить, что переданная строка является доменом http://site.com', function () {
        assert.deepEqual(reg.task48('http://site.com'), true);
    });
});

describe('regex49', function () {
    it('Определить, что переданная строка является доменом http://site.ru', function () {
        assert.deepEqual(reg.task49('http://site.ru'), true);
    });
    it('Определить, что переданная строка является доменом my-site.com', function () {
        assert.deepEqual(reg.task49('my-site.com'), false);
    });
    it('Определить, что переданная строка является доменом https://site.com', function () {
        assert.deepEqual(reg.task49('https://site.com'), true);
    });
});

describe('regex50', function () {
    it('Определить, что переданная строка является доменом http://site.ru', function () {
        assert.deepEqual(reg.task50('http://site.ru'), true);
    });
    it('Определить, что переданная строка является доменом my-site.com', function () {
        assert.deepEqual(reg.task50('my-site.com'), false);
    });
});

describe('regex51', function () {
    it('Определить, что переданная строка заканчивается расширением txt, html или php: index.html', function () {
        assert.deepEqual(reg.task51('index.html'), true);
    });
    it('Определить, что переданная строка заканчивается расширением txt, html или php: imagine', function () {
        assert.deepEqual(reg.task51('imagine'), false);
    });
    it('Определить, что переданная строка заканчивается расширением txt, html или php: crud.php', function () {
        assert.deepEqual(reg.task51('crud.php'), true);
    });
    it('Определить, что переданная строка заканчивается расширением txt, html или php: note.txt', function () {
        assert.deepEqual(reg.task51('note.txt'), true);
    });
});

describe('regex52', function () {
    it('Определить, что переданная строка заканчивается расширением jpg или jpeg: index.html', function () {
        assert.deepEqual(reg.task52('index.html'), false);
    });
    it('Определить, что переданная строка заканчивается расширением jpg или jpeg: imagine.jpg', function () {
        assert.deepEqual(reg.task52('imagine.jpg'), true);
    });
    it('Определить, что переданная строка заканчивается расширением jpg или jpeg: crud.jpeg', function () {
        assert.deepEqual(reg.task52('crud.jpeg'), true);
    });
});

describe('regex53', function () {
    it('Узнать является ли строка числом, длиной до 12 цифр: 536748356409854457', function () {
        assert.deepEqual(reg.task53('536748356409854457'), false);
    });
    it('Узнать является ли строка числом, длиной до 12 цифр: 536748', function () {
        assert.deepEqual(reg.task53('536748'), true);
    });
});

describe('regex54', function () {
    it('Найти сумму всех чисел из данной строки: ha 7 j 54 jfk a#a 4 ha 8', function () {
        assert.deepEqual(reg.task54('ha 7 j 54 jfk a#a 4 ha 8'), 28);
    });
    it('Найти сумму всех чисел из данной строки: br 1 f 4 hf3 j6d jd23', function () {
        assert.deepEqual(reg.task54('br 1 f 4 hf3 j6d jd23'), 19);
    });
});

describe('regex55', function () {
    it('Заменить в строке домен вида http://site.ru на <a href="http://site.ru">site.ru</a>', function () {
        assert.deepEqual(reg.task55('http://site.ru'), '<a href="http://site.ru">site.ru</a>');
    });
});

describe('regex56', function () {
    it('Заменить все повторяющиеся пробелы на один: ts  43   jdk    tey', function () {
        assert.deepEqual(reg.task56('ts  43   jdk    tey'), 'ts 43 jdk tey');
    });
});

describe('regex57', function () {
    it('Найти и удалить все комментарии CSS', function () {
        assert.deepEqual(reg.task57('/*add styles*/'), '');
    });
});

describe('regex58', function () {
    it('Найти и удалить все комментарии HTML', function () {
        assert.deepEqual(reg.task58('//add a new container'), '');
    });
});

describe('regex59', function () {
    it('Из \'aaab\' нужно сделать \'!b\'', function () {
        assert.deepEqual(reg.task59('aaab aawa aaaw'), '!b aawa aaaw');
    });
});

describe('regex60', function () {
    it('Из \'aaaw\' нужно сделать \'!w\'', function () {
        assert.deepEqual(reg.task60('aaab aawa aaaw'), 'aaab aawa !w');
    });
});

describe('regex61', function () {
    it('Преобразовать строку так, чтобы вместо целых чисел стояли их квадраты: 2 3 10 9', function () {
        assert.deepEqual(reg.task61('2 3 10 9'), '4 9 100 81');
    });
});

describe('regex62', function () {
    it('Найти числа, стоящие в кавычках и увеличьте их в два раза', function () {
        assert.deepEqual(reg.task62('2aaa\'3\'bbb\'4\''), '2aaa\'6\'bbb\'8\'');
    });
});

describe('regex63', function () {
    it('Найти все такие {{текст}} вставки и поменяйте в них порядок букв на обратный', function () {
        assert.deepEqual(reg.task63('Найти все такие {{текст}} вставки'), 'Найти все такие {{тскет}} вставки');
    });
});

describe('regex64', function () {
    it('Дана строка \'23 + 35 =\'. Выведите на экран результат операции в виде \'23 + 35 = 58\'', function () {
        assert.deepEqual(reg.task64('23 + 35 ='), '23 + 35 =58');
    });
});

describe('regex65', function () {
    it('Определить, что год находится в интервале от 1900 до 2100', function () {
        assert.deepEqual(reg.task65('1999'), true);
    });
    it('Определить, что год находится в интервале от 1900 до 2100', function () {
        assert.deepEqual(reg.task65('2200'), false);
    });
});

describe('regex66', function () {
    it('Определить, что переданная строка является корректным временем 23:41', function () {
        assert.deepEqual(reg.task66('23:41'), true);
    });
    it('Определить, что переданная строка является корректным временем 12.93', function () {
        assert.deepEqual(reg.task66('12.93'), false);
    });
});

describe('regex67', function () {
    it('Определить, что переданная строка является корректным временем 9.59 am', function () {
        assert.deepEqual(reg.task67('9.59 am'), true);
    });
    it('Определить, что переданная строка является корректным временем 12.93', function () {
        assert.deepEqual(reg.task67('59.59 am'), false);
    });
});

describe('regex68', function () {
    it('Удалить все слова из предложения, содержащие две одинаковые следующие друг за другом буквы: aaaab ssst opppp kilo map', function () {
        assert.deepEqual(reg.task68('aaaab ssst opppp kilo map'), 'kilo map');
    });
});

describe('regex69', function () {
    it('Удалить все повторяющиеся слова из строки \'dsf xxx xxx sd\'', function () {
        assert.deepEqual(reg.task69('dsf xxx xxx sd'), 'dsf xxx sd');
    });
});

describe('regex70', function () {
    it('Удалить все повторяющиеся слова из строки \'dsf xxx xxx xxx xxx xxx sd\'', function () {
        assert.deepEqual(reg.task70('dsf xxx xxx xxx xxx xxx sd'), 'dsf xxx sd');
    });
});