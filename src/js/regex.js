'use strict'

// На '.'
function task1 (str) {
    return str.match(/a.b/g);

};

function task2 (str) {
    return str.match(/a..a/g);

};

function task3 (str) {
    return str.match(/ab.a/g);

};

// На +, *, ?, ()
function task4 (str) {
    return str.match(/ab+a/g);

};

function task5 (str) {
    return str.match(/ab*a/g);

};

function task6 (str) {
    return str.match(/ab?a/g);

};

function task7 (str) {
    return str.match(/a(b|b*)a/g);

};

function task8 (str) {
    return str.match(/(ab)+/g);

};

function task9 (str) {
    return str.match(/a\.a/g);

};

function task10 (str) {
    return str.match(/\d\+\d/g);

};

function task11 (str) {
    return str.match(/\d\++\d/g);

};

function task12 (str) {
    return str.match(/[2]\+*[3]/g);

};

function task13 (str) {
    return str.match(/\*q+\+/g);

};

// На жадность
function task14 (str) {
    let arr = str.match(/a.+?a/g).join(', ');
    let newStr = arr.replace(/a/g, '!');
    return newStr;

};

// Ha {}
function task15 (str) {
    return str.match(/ab{2,4}a/g);

};

function task16 (str) {
    return str.match(/ab{0,3}a/g);

};

function task17 (str) {
    return str.match(/ab{4,5}a/g);

};

// Ha \s, \S, \w, \W, \d, \D
function task18 (str) {
    return str.match(/a\da/g);

};

function task19 (str) {
    return str.match(/a\d+a/g);

};

function task20 (str) {
    return str.match(/a\d*a/g);

};

function task21 (str) {
    return str.match(/a[\D*]b/g);

};

function task22 (str) {
    return str.match(/a\Wb/g);

};

function task23 (str) {
    return str.replace(/\s/g, '!');

};

// Ha [], ^, [a-zA-Z]
function task24 (str) {
    return str.match(/a[b,e,x]a/g);

};

function task25 (str) {
    return str.match(/a[b,\.,\+,\*]a/g);

};

function task26 (str) {
    return str.match(/a[3-7]a/g);

};

function task27 (str) {
    return str.match(/a[a-g]a/g);

};

function task28 (str) {
    return str.match(/a[a-fj-z]a/g);

};

function task29 (str) {
    return str.match(/a[a-fA-Z]a/g);
    
};

function task30 (str) {
    return str.match(/a[^e,x\s]a/g);

};

function task31 (str) {
    return str.match(/w[а-яё]w/g);

};

// Ha [a-zA-Z] и квантификаторы
function task32 (str) {
    return str.match(/a[a-z]+a/g);

};

function task33 (str) {
    return str.match(/a[a-zA-Z]+a/g);

};

function task34 (str) {
    return str.match(/a[a-z0-9]+a/g);

};

function task35 (str) {
    return str.match(/[а-яА-ЯёЁ]+/g);

};

// Ha ^, $
function task36 (str) {
    return str.replace(/^a{3}/g, '!');

};

function task37 (str) {
    return str.replace(/a{3}$/g, '!');

};

//Ha '|'
function task38 (str) {
    return str.match(/ae+a|ax+a/g);

};

function task39 (str) {
    return str.match(/ae{2}a|ax+a/g);

};

// Ha \
function task40 (str) {
    return str.replace(/a\\a/g, '!');

};

function task41 (str) {
    return str.replace(/a\\\\a/g, '!');

};

//На экранировку посложнее
function task42 (str) {
    return str.replace(/\/.{3}\\/g, '!');

};

//На карманы при замене
function task43 (str) {
    return str.replace(/(\w+)@(\w+)/g, '$2@$1');

};

function task44 (str) {
    return str.replace(/(\d)/g, '$1$1');

};

// Задачи на test и match
function task45 (str) {
    return /(\w|\.|\-)+@[a-zA-Z]+\.[a-zA-Z]+/.test(str);

};

function task46 (str) {
    return str.match(/[a-z]+@[a-z]+\.[a-z]{2,3}|[a-z]+.[a-z]+@[a-z]+\.[a-z]{2,3}/g);

};

function task47 (str) {
    return /^[a-z]+\.[a-z]{2,3}$|^[a-z]+.[a-z]+\.[a-z]{2,3}$/.test(str);

};

function task48 (str) {
    return /^http:\/\/[a-z]+\.[a-z]{2,3}$/.test(str);

};

function task49 (str) {
    return /^http:\/\/[a-z]+\.[a-z]{2,3}$|^https:\/\/[a-z]+\.[a-z]{2,3}$/.test(str);

};

function task50 (str) {
    return /^http|^https/.test(str);

};

function task51 (str) {
    return /txt$|html$|php$/.test(str);

};

function task52 (str) {
    return /jpg$|jpeg$/.test(str);

};

function task53 (str) {
    return /^\d{1,11}$/.test(str);

};

function task54 (str) {
    let arr = str.match(/\d/g);
    let sum = 0;
    for (let i = 0; i < arr.length; i++) {
        sum += parseInt(arr[i]);
    }
    return sum;

};


//Задачи на replace
function task55 (str) {
    return str.replace(/http:\/\/site\.[a-z]{2,3}/g, "<a href=\"http:\/\/site\.ru\">site\.ru<\/a>");

};

function task56 (str) {
    return str.replace(/\s+/g, " ");

};

function task57 (str) {
    return str.replace(/\/\*.*?\*\//g, "");

};

function task58 (str) {
    return str.replace(/\/\/.*/g, "");

};


//На позитивный и негативный просмотр
function task59 (str) {
    return str.replace(/a{3}(?=b)/, '!');
};

function task60 (str) {
    return str.replace(/a{3}(?!b)/, '!');

};


//На replace
function task61 (str) {
    return str.replace(/\d+/g, function (match) {
        return match * match;
    });

};

function task62 (str) {
    return str.replace(/\d+(?=')/g, function (match) {
        return match * 2;
    });

};

function task63 (str) {
    return str.replace(/(т)(е)(к)(с)(т)(?=}})/g, '$5$4$3$2$1');

};

function task64 (str) {
    return str.replace(/(\d+)\s\+\s(\d+)\s=/g, function (match, match1, match2) {
        let res = parseInt(match1) + parseInt(match2);
        return match + res;
    });

};

function task65 (str) {
    return (/^(19\d\d|20\d\d|2100)$/.test(str));

};

function task66 (str) {
    return /^([01]\d|2[0-3]):[0-5]\d$/.test(str);

};

function task67 (str) {
    return /^(\d|[10-12]\d)\.[0-5]\d\s[ap]m$/.test(str);

};

function task68 (str) {
    return str.replace(/\W*\w*(\w)\1\w*\W*/g, "");

};

function task69 (str) {
    return str.replace(/\b(\w+)\s+\1/g, "$1");

};

function task70 (str) {
    return str.replace(/\b(\w+)\b(?:\s+\1\b)+/g, "$1");

};

module.exports = {
    task1, task2, task3, task4,task5, task6, task7, task8, task9, task10,
    task11, task12, task13, task14, task15, task16, task17, task18, task19, task20,
    task21, task22, task23, task24, task25, task26, task27, task28, task29, task30,
    task31, task32, task33, task34, task35, task36, task37, task38, task39, task40,
    task41, task42, task43, task44, task45, task46, task47, task48, task49, task50,
    task51, task52, task53, task54, task55, task56, task57, task58, task59, task60,
    task61, task62, task63, task64, task65, task66, task67, task68, task69, task70
};